import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonBeta} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  environment: "beta",
  enablePiwikTrack: true,
  adminToolsAPIURL: "https://beta.services.openaire.eu/uoa-monitor-service/",
  adminToolsPortalType: "monitor",
  adminToolsCommunity: "monitor",
  domain : "https://beta.monitor.openaire.eu",
  afterLoginRedirectLink: '/my-dashboards',
};

export let properties: EnvProperties = {
  ...common, ...commonBeta, ...props
}
