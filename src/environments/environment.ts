import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonDev} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  environment: "development",
  enablePiwikTrack: false,
  useCache: false,
  showContent: true,
  adminToolsAPIURL: "http://mpagasas.di.uoa.gr:19380/uoa-monitor-service/",
  adminToolsPortalType: "monitor",
  adminToolsCommunity: "monitor",
  domain : "http://mpagasas.di.uoa.gr:4000",
  afterLoginRedirectLink: '/my-dashboards',
};

export let properties: EnvProperties = {
  ...common, ...commonDev, ...props
}
