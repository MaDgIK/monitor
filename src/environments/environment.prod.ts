import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonProd} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  environment: "production",
  enablePiwikTrack: true,
  adminToolsAPIURL: "https://services.openaire.eu/uoa-monitor-service/",
  adminToolsPortalType: "monitor",
  adminToolsCommunity: "monitor",
  domain : "https://monitor.openaire.eu",
  afterLoginRedirectLink: '/my-dashboards',
};

export let properties: EnvProperties = {
  ...common, ...commonProd, ...props
}
