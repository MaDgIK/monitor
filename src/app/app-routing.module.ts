import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OpenaireErrorPageComponent} from './error/errorPage.component';

const routes: Routes = [
  {path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule), data: {hasStickyHeaderOnMobile: true}},
  {path: 'about/learn-how', redirectTo: 'about', pathMatch: 'full'},
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then(m => m.AboutModule)
  },
  {
    path: 'subscriptions',
    loadChildren: () => import('./subscriptions/subscriptions.module').then(m => m.SubscriptionsModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./support/support.module').then(m => m.SupportModule),
    data: {hasStickyHeaderOnMobile: true}
  },
  {
    path: 'methodology',
    loadChildren: () => import('./openaireLibrary/monitor/methodology/methodology.module').then(m => m.MethodologyModule)
  },
  {
    path: 'indicators',
    loadChildren: () => import('./openaireLibrary/monitor/indicators/indicators.module').then(m => m.IndicatorsModule)
  },
  {
    path: 'browse',
    loadChildren: () => import('./search-stakeholders/search-stakeholders.module').then(m => m.SearchStakeholdersModule)
  },
  {
    path: 'contact-us',
    loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule),
    data: {hasQuickContact: false}
  },
	{
		path: 'get-started',
		loadChildren: () => import('./get-started/get-started.module').then(m => m.GetStartedModule)
	},
  {
    path: 'my-dashboards',
    loadChildren: () => import('./my-stakeholders/my-stakeholders.module').then(m => m.MyStakeholdersModule)},
  {
    path: 'reload',
    loadChildren: () => import('./reload/libReload.module').then(m => m.LibReloadModule)
  },
  {
    path: 'user-info',
    loadChildren: () => import('./login/libUser.module').then(m => m.LibUserModule)
  },
  {path: 'error', component: OpenaireErrorPageComponent},
  {path: 'theme', loadChildren: () => import('./openaireLibrary/utils/theme/theme.module').then(m => m.ThemeModule)},
  {
    path: '**',
    pathMatch: 'full',
    component: OpenaireErrorPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: "reload"
})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
