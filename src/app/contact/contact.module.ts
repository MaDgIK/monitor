import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {ContactComponent} from './contact.component';
import {ContactRoutingModule} from "./contact-routing.module";
import {EmailService} from "../openaireLibrary/utils/email/email.service";
import {RecaptchaModule} from "ng-recaptcha";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {ContactUsModule} from "../openaireLibrary/contact-us/contact-us.module";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";


@NgModule({
  imports: [
    ContactRoutingModule, CommonModule, RouterModule,
    AlertModalModule, RecaptchaModule, HelperModule,
    Schema2jsonldModule, SEOServiceModule, ContactUsModule, BreadcrumbsModule
  ],
  declarations: [
    ContactComponent
  ],
  providers: [
    EmailService, PiwikService, IsRouteEnabled
  ],
  exports: [
    ContactComponent
  ]
})

export class ContactModule { }
