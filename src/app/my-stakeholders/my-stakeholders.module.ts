import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ManageModule} from '../openaireLibrary/utils/manage/manage.module';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {ErrorMessagesModule} from '../openaireLibrary/utils/errorMessages.module';
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {MyStakeholdersRoutingModule} from "./my-stakeholders-routing.module";
import {MyStakeholdersComponent} from "./my-stakeholders.component";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {BrowseStakeholderModule} from "../browse-stakeholder/browse-stakeholder.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, HelperModule,
    ManageModule, ErrorMessagesModule, Schema2jsonldModule, SEOServiceModule,
    MyStakeholdersRoutingModule, BreadcrumbsModule, LoadingModule, BrowseStakeholderModule
  ],
  declarations: [
    MyStakeholdersComponent
  ],
  providers: [
    LoginGuard, PreviousRouteRecorder, PiwikService
  ],
  exports: [
    MyStakeholdersComponent
  ]
})
export class MyStakeholdersModule {
}
