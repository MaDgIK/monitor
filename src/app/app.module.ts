import {APP_ID, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from './app.component';
import {OpenaireErrorPageComponent} from './error/errorPage.component';

import {AppRoutingModule} from './app-routing.module';
import {CookieLawModule} from './openaireLibrary/sharedComponents/cookie-law/cookie-law.module';
import {BottomModule} from './openaireLibrary/sharedComponents/bottom.module';
import {ErrorModule} from './openaireLibrary/error/error.module';
import {NavigationBarModule} from './openaireLibrary/sharedComponents/navigationBar.module';
import {QuickContactModule} from './openaireLibrary/sharedComponents/quick-contact/quick-contact.module';
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./openaireLibrary/timeout-interceptor.service";
import {Schema2jsonldModule} from "./openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {HttpInterceptorService} from "./openaireLibrary/http-interceptor.service";
import {ErrorInterceptorService} from "./openaireLibrary/error-interceptor.service";
import {SharedModule} from "./openaireLibrary/shared/shared.module";
import {AlertModalModule} from "./openaireLibrary/utils/modal/alertModal.module";
import {isDevelopmentGuard} from './openaireLibrary/error/isDevelopmentGuard.guard';
import {HelperModule} from "./openaireLibrary/utils/helper/helper.module";

@NgModule({

  imports: [
    SharedModule,
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule,
    ErrorModule,
    FormsModule,
    NavigationBarModule,
    QuickContactModule,
    BottomModule,
    CookieLawModule,
    BrowserModule,
    AppRoutingModule,
    Schema2jsonldModule,
    AlertModalModule,
    HelperModule
  ],
  declarations: [AppComponent, OpenaireErrorPageComponent],
  exports: [AppComponent],
  providers: [
		isDevelopmentGuard,
    {provide: APP_ID, useValue: 'monitor'},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    },
    [{provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true}],
    [{provide: DEFAULT_TIMEOUT, useValue: 30000}]
  ],
  bootstrap: [AppComponent]
})
//
export class AppModule {
}
