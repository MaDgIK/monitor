import {ChangeDetectorRef, Component, ViewChild} from "@angular/core";
import {SearchUtilsClass} from "../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {ErrorMessagesComponent} from "../openaireLibrary/utils/errorMessages.component";
import {ErrorCodes} from "../openaireLibrary/utils/properties/errorCodes";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {ActivatedRoute} from "@angular/router";
import {AdvancedField, Filter, Value} from "../openaireLibrary/searchPages/searchUtils/searchHelperClasses.class";
import {SearchFields} from "../openaireLibrary/utils/properties/searchFields";
import {Session, User} from "../openaireLibrary/login/utils/helper.class";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {StakeholderService} from "../openaireLibrary/monitor/services/stakeholder.service";
import {NewSearchPageComponent, SearchForm} from "../openaireLibrary/searchPages/searchUtils/newSearchPage.component";
import {StakeholderInfo, StakeholderType} from "../openaireLibrary/monitor/entities/stakeholder";
import {properties} from "../../environments/environment";
import {Subscriber} from "rxjs";
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {TitleCasePipe} from "@angular/common";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";
import {StakeholderUtils} from "../openaireLibrary/monitor-admin/utils/indicator-utils";

@Component({
  selector: 'search-stakeholders',
  template: `
    <new-search-page pageTitle="OpenAIRE-Monitor | Browse {{stakeholderUtils.entities.stakeholders}}"
                     [hasPrefix]=false
                     [formPlaceholderText]="'Search ' + (!isMobile?'OpenAIRE Monitor ':'') +stakeholderUtils.entities.stakeholders"
                     [type]="(results.length > 1) ? stakeholderUtils.entities.stakeholders:stakeholderUtils.entities.stakeholders"
                     entityType="stakeholder"
                     [results]="results" [searchUtils]="searchUtils"
                     [showResultCount]=true
                     [disableForms]="disableForms"
                     [showIndexInfo]=false
                     [simpleView]="true"
                     [searchForm]="searchForm"
                     [fieldIds]="fieldIds" [fieldIdsMap]="fieldIdsMap" [selectedFields]="selectedFields"
                     [showBreadcrumb]="true" [breadcrumbs]="breadcrumbs"
                     [simpleSearchLink]="searchLink" [entitiesSelection]="false">
    </new-search-page>
  `
})
export class SearchStakeholdersComponent {
  private errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;
  public results: StakeholderInfo[] = [];
  public totalResults: StakeholderInfo[];
  public stakeholderUtils: StakeholderUtils = new StakeholderUtils();
  public subscriptions = [];
  public filters = [];
  public searchFields: SearchFields = new SearchFields();
  public searchUtils: SearchUtilsClass = new SearchUtilsClass();
  public selectedFields: AdvancedField[] = [];
  public disableForms: boolean = false;
  public baseUrl: string = null;
  public fieldIds: string[] = ["q"];
  public refineFields: string[] = this.searchFields.STAKEHOLDER_SEARCH_FIELDS;
  public fieldIdsMap = {
    ["q"]: {
      name: "All fields",
      type: "keyword",
      param: "q",
      operator: "op",
      equalityOperator: "=",
      filterType: null
    }
  };
  public keyword = "";
  public searchLink;
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Browse ' + this.stakeholderUtils.entities.stakeholders}];
  public searchForm: SearchForm = {class: 'uk-background-muted', dark: false};
  public isMobile: boolean = false;
  properties: EnvProperties = properties;
  @ViewChild(NewSearchPageComponent, {static: true}) searchPage: NewSearchPageComponent;
  private user: User;
  
  constructor(private route: ActivatedRoute,
              private _stakeholderService: StakeholderService,
              private userManagementService: UserManagementService,
              public titleCasePipe: TitleCasePipe,
              private layoutService: LayoutService,
              private cdr: ChangeDetectorRef) {
    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.searchUtils.status = this.errorCodes.LOADING;
  }
  
  public ngOnInit() {
    this.baseUrl = this.properties.searchLinkToStakeholders;
    this.subscriptions.push(this.route.queryParams.subscribe(params => {
      this.searchPage.resultsPerPage = 10;
      this.keyword = (params['fv0'] ? params['fv0'] : '');
      this.keyword = StringUtils.URIDecode(this.keyword);
      this.searchUtils.page = (params['page'] === undefined) ? 1 : +params['page'];
      this.searchUtils.sortBy = (params['sortBy'] === undefined) ? '' : params['sortBy'];
      this.searchUtils.validateSize(params['size']);
      this.searchUtils.baseUrl = this.baseUrl;
      this.searchPage.searchUtils = this.searchUtils;
      if (this.searchUtils.sortBy && this.searchUtils.sortBy != "creationdate,descending" && this.searchUtils.sortBy != "creationdate,ascending") {
        this.searchUtils.sortBy = "";
      }
      this.searchPage.refineFields = this.refineFields;
      this.searchLink = this.properties.searchLinkToStakeholders;
      this.selectedFields = [];
      this.searchPage.prepareSearchPage(this.fieldIds, this.selectedFields, this.refineFields, [], [], this.fieldIdsMap, null, params, "stakeholder", null);
      let queryParams = params;
      if (typeof document !== 'undefined') {
        this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
          this.user = user;
          this.initResults(queryParams);
        }));
      } else {
        this.initResults(queryParams);
      }
    }));
    this.layoutService.isMobile.subscribe(isMobile => {
      this.isMobile = isMobile;
      this.cdr.detectChanges();
    })
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  /**
   * Initialize stakeholders from Communities APIs
   *
   * @param params
   */
  private initResults(params) {
    if (this.totalResults) {
      this.totalResults = StakeholderInfo.toStakeholderInfo(this.totalResults, this.user);
      this._getResults(params);
    } else {
      this.subscriptions.push(this._stakeholderService.getStakeholders().subscribe(
        data => {
          this.totalResults = StakeholderInfo.toStakeholderInfo(data, this.user);
          this._getResults(params);
        },
        err => {
          this.handleError('Error getting stakeholders', err);
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
          this.disableForms = false;
        }
      ));
    }
  }
  
  
  /**
   * Get all stakeholders from mock API and apply permission access validator,
   * keyword searching, filter, paging and sorting.
   *
   * @param params, status
   * @private
   */
  private _getResults(params) {
    this.searchUtils.status = this.errorCodes.LOADING;
    this.disableForms = true;
    this.results = this.totalResults;
    this.filters = this.createFilters();
    this.searchUtils.totalResults = 0;
    this.applyParams(params);
  }
  
  /**
   * Apply permission access validator,
   * keyword searching, filter, paging and sorting.
   *
   * @param params
   * @param status
   */
  public applyParams(params) {
    if (this.keyword && this.keyword != '') {
      this.searchForKeywords();
    }
    this.checkFilters(params);
    this.sort();
    this.searchUtils.totalResults = this.results.length;
    this.filters = this.searchPage.prepareFiltersToShow(this.filters, this.searchUtils.totalResults);
    this.results = this.results.slice((this.searchUtils.page - 1) * this.searchUtils.size, (this.searchUtils.page * this.searchUtils.size));
    this.searchUtils.status = this.errorCodes.DONE;
    if (this.searchUtils.totalResults == 0) {
      this.searchUtils.status = this.errorCodes.NONE;
    }
    this.disableForms = false;
    if (this.searchUtils.status == this.errorCodes.DONE) {
      // Page out of limit!!!
      let totalPages: any = this.searchUtils.totalResults / (this.searchUtils.size);
      if (!(Number.isInteger(totalPages))) {
        totalPages = (parseInt(totalPages, 10) + 1);
      }
      if (totalPages < this.searchUtils.page) {
        this.searchUtils.totalResults = 0;
        this.searchUtils.status = this.errorCodes.OUT_OF_BOUND;
      }
    }
  }
  
  
  /**
   * Parse the given keywords into array and check if any of the requirements field of a funder includes
   * one of the given words.
   */
  private searchForKeywords() {
    let ret = [];
    let keywords: string[] = this.keyword.split(/,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/, -1);
    for (let i = 0; i < this.results.length; i++) {
      for (let keyword of keywords) {
        keyword = keyword.toLowerCase();
        if (keyword != '' && (StringUtils.containsWord(this.results[i].name, keyword) || StringUtils.containsWord(this.results[i].index_shortName, keyword) ||
          StringUtils.containsWord(this.results[i].alias, keyword) || StringUtils.containsWord(this.results[i].description, keyword))) {
          ret.push(this.results[i]);
          break;
        }
      }
    }
    this.results = ret;
  }
  
  /**
   * Check the current results if they satisfy the values of each filter category and
   * update the number of possible results in each value.
   *
   * @param params
   */
  private checkFilters(params) {
    let typeResults: StakeholderInfo[] = this.applyFilter('type', params);
    let accessResults: StakeholderInfo[] = this.applyFilter('access', params);
    let roleResults: StakeholderInfo[] = this.results;
    if (this.user) {
      roleResults = this.applyFilter('role', params);
      this.resetFilterNumbers('role');
      this.updateFilterNumbers(accessResults.filter(value => {
        return typeResults.includes(value);
      }), 'role');
    }
    this.resetFilterNumbers('access');
    this.updateFilterNumbers(typeResults.filter(value => {
      return roleResults.includes(value);
    }), 'access');
    this.resetFilterNumbers('type');
    this.updateFilterNumbers(accessResults.filter(value => {
      return roleResults.includes(value);
    }), 'type');
    this.results = accessResults.filter(value => {
      return typeResults.includes(value);
    })
    this.results = this.results.filter(value => {
      return roleResults.includes(value);
    });
  }
  
  /**
   * Apply filter with filterId and return the results
   *
   * @param filterId
   * @param params
   */
  private applyFilter(filterId: string, params): StakeholderInfo[] {
    let results = [];
    let values: string[] = [];
    if (params[filterId]) {
      values = (StringUtils.URIDecode(params[filterId])).split(/,(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)/, -1);
    }
    if (filterId == 'type') {
      for (let i = 0; i < this.results.length; i++) {
        if (values.length == 0) {
          results.push(this.results[i]);
        } else {
          for (let value of values) {
            if (this.results[i].type == value.replace(/["']/g, "")) {
              results.push(this.results[i]);
              break;
            }
          }
        }
      }
    } else if (filterId == 'access') {
      for (let i = 0; i < this.results.length; i++) {
        if (values.length == 0) {
          results.push(this.results[i]);
        } else {
          for (let value of values) {
            if (value.replace(/["']/g, "") == 'public') {
              if (this.results[i].visibility === 'PUBLIC') {
                results.push(this.results[i]);
                break;
              }
            } else if (value.replace(/["']/g, "") == 'restricted') {
              if (this.results[i].visibility === 'RESTRICTED') {
                results.push(this.results[i]);
                break;
              }
            } else if (value.replace(/["']/g, "") == 'private') {
              if (this.results[i].visibility === 'PRIVATE') {
                results.push(this.results[i]);
                break;
              }
            }
          }
        }
      }
    } else if (filterId == 'role') {
      for (let i = 0; i < this.results.length; i++) {
        if (values.length == 0) {
          results.push(this.results[i]);
        } else {
          for (let value of values) {
            if (value.replace(/["']/g, "") == 'manager') {
              if (this.results[i].isManager) {
                results.push(this.results[i]);
                break;
              }
            }
            if (value.replace(/["']/g, "") == 'member') {
              if (this.results[i].isMember) {
                results.push(this.results[i]);
                break;
              }
            }
          }
        }
      }
    }
    return results;
  }
  
  /**
   * Reset the values of filter with id filterId with zero.
   *
   * @param filterId
   */
  private resetFilterNumbers(filterId: string) {
    for (let i = 0; i < this.filters.length; i++) {
      if (this.filters[i].filterId == filterId) {
        for (let j = 0; j < this.filters[i].values.length; j++) {
          this.filters[i].values[j].number = 0;
        }
        break;
      }
    }
  }
  
  /**
   * Update the values of filter with id filterId based on
   * results.
   *
   * @param results
   * @param filterId
   */
  private updateFilterNumbers(results: StakeholderInfo[], filterId: string) {
    for (let k = 0; k < results.length; k++) {
      for (let i = 0; i < this.filters.length; i++) {
        if (this.filters[i].filterId == filterId) {
          if (this.filters[i].filterId == 'type') {
            for (let j = 0; j < this.filters[i].values.length; j++) {
              if (results[k].type == this.filters[i].values[j].id) {
                this.filters[i].values[j].number++;
              }
            }
          } else if (this.filters[i].filterId == 'access') {
            if (results[k].visibility === 'PUBLIC') {
              this.filters[i].values[0].number++;
            } else if (results[k].visibility === 'RESTRICTED') {
              this.filters[i].values[1].number++;
            } else if (this.user) {
              this.filters[i].values[2].number++;
            }
          } else if (this.filters[i].filterId == 'role') {
            if (results[k].isManager) {
              this.filters[i].values[0].number++;
            }
            if (results[k].isMember) {
              this.filters[i].values[1].number++;
            }
          }
          break;
        }
      }
    }
  }
  
  /**
   * Sorting results based on sortBy.
   */
  private sort() {
    if (this.searchUtils.sortBy == '') {
      this.results.sort((left, right): number => {
        if (left.name > right.name) {
          return 1;
        } else if (left.name < right.name) {
          return -1;
        } else {
          return 0;
        }
      })
    } else if (this.searchUtils.sortBy == 'creationdate,descending') {
      this.results.sort((left, right): number => {
        if (!right.creationDate || left.creationDate > right.creationDate) {
          return -1;
        } else if (!left.creationDate || left.creationDate < right.creationDate) {
          return 1;
        } else {
          return 0;
        }
      })
    } else if (this.searchUtils.sortBy == 'creationdate,ascending') {
      this.results.sort((left, right): number => {
        if (!right.creationDate || left.creationDate > right.creationDate) {
          return 1;
        } else if (!left.creationDate || left.creationDate < right.creationDate) {
          return -1;
        } else {
          return 0;
        }
      })
    }
  }

  get types(): StakeholderType[] {
    return this.stakeholderUtils.types.map(option => option.value).filter(type => this.totalResults.findIndex(stakeholder => stakeholder.type === type) > -1);
  }
  
  /**
   * Create Search Stakeholder filters.
   *
   */
  private createFilters(): Filter[] {
    let filter_names = [];
    let filter_ids = [];
    let value_names = [];
    let value_original_ids = [];
    filter_names.push("Type");
    filter_ids.push("type");
    value_names.push(this.types.map(type => this.stakeholderUtils.entities[type + 's']));
    value_original_ids.push(this.types);
    filter_names.push("Accessibility");
    filter_ids.push("access");
    if (!this.user) {
      value_names.push(["Public", "Restricted"]);
      value_original_ids.push(["public", "restricted"]);
    } else {
      value_names.push(["Public", "Restricted", "Private"]);
      value_original_ids.push(["public", "restricted", "private"]);
      filter_names.push("Role");
      filter_ids.push("role");
      value_names.push(["Manager", "Member"]);
      value_original_ids.push(["manager", "member"]);
    }
    let filters: Filter[] = [];
    for (let i = 0; i < filter_names.length; i++) {
      let values: Value[] = [];
      for (let j = 0; j < value_names[i].length; j++) {
        let value: Value = {name: value_names[i][j], id: value_original_ids[i][j], number: 0, selected: false};
        values.push(value);
      }
      let filter: Filter = {
        title: filter_names[i],
        filterId: filter_ids[i],
        originalFilterId: this.refineFields[i],
        values: values,
        countSelectedValues: 0,
        filterOperator: 'or',
        valueIsExact: true,
        filterType: "checkbox"
      };
      filters.push(filter);
    }
    return filters;
  }
  
  private handleError(message: string, error) {
    console.error('Communities Search Page: ' + message, error);
  }
  
}
