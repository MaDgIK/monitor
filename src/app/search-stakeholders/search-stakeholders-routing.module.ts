import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SearchStakeholdersComponent} from "./search-stakeholders.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: SearchStakeholdersComponent, canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class SearchStakeholdersRoutingModule { }
