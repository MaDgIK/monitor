import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {GetStartedComponent} from './get-started.component';
import {GetStartedRoutingModule} from "./get-started-routing.module";
import {EmailService} from "../openaireLibrary/utils/email/email.service";
import {RecaptchaModule} from "ng-recaptcha";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {ContactUsModule} from "../openaireLibrary/contact-us/contact-us.module";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';


@NgModule({
  imports: [
    GetStartedRoutingModule, CommonModule, RouterModule,
    AlertModalModule, RecaptchaModule, HelperModule,
    Schema2jsonldModule, SEOServiceModule, ContactUsModule, BreadcrumbsModule, LoadingModule,
		IconsModule
  ],
  declarations: [
    GetStartedComponent
  ],
  providers: [
    EmailService, PiwikService, IsRouteEnabled
  ],
  exports: [
    GetStartedComponent
  ]
})

export class GetStartedModule { }
