import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {SupportComponent} from "./support.component";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";

@NgModule({
	declarations: [SupportComponent],
	imports: [CommonModule, RouterModule.forChild([
		{
			path: '',
			component: SupportComponent,
			data: {extraOffset: 50}
		}
	]), BreadcrumbsModule, HelperModule],
	exports: [SupportComponent],
	providers: [PreviousRouteRecorder, PiwikService]
})
export class SupportModule {}
