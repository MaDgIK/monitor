import {Component} from "@angular/core";
import {Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {Subscriber} from "rxjs";
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../environments/environment";
import {BaseComponent} from "../openaireLibrary/sharedComponents/base/base.component";
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";

@Component({
  selector: 'subscriptions',
  template: `
      <div>
          <div class="uk-position-relative">
              <div class="uk-background-muted">
                  <div class="uk-container uk-container-large uk-section uk-section-small">
                      <div class="uk-padding-small uk-padding-remove-horizontal">
                          <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
                      </div>
                      <div class="uk-flex uk-flex-column uk-flex-center uk-margin-bottom"
                           uk-scrollspy="target: [uk-scrollspy-class]; cls: uk-animation-fade; delay: 250">
                          <h1 class="uk-margin-medium-top" uk-scrollspy-class>Subscriptions<span
                                  class="uk-text-primary">.</span></h1>
                          <div uk-scrollspy-class>
                              As demand for our services grows, along with the features included in each of them,
                              traditional sources of funding may not be sufficient. By introducing subscription fees, we
                              can ensure their long-term sustainability; by achieving financial stability we can
                              continue to invest in better resources, technology, and talent, thereby increasing our
                              capacity to deliver impactful programs and services to all interested parties.
                              Subscriptions support immediate operational needs, while at the same time enabling us to
                              scale our efforts and make a greater, more sustainable difference in the communities we
                              serve.
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <helper *ngIf="divContents" [texts]="divContents.plans"></helper>
      </div>
  `
})
export class SubscriptionsComponent extends BaseComponent {
  public divContents = null;

  public title: string = "OpenAIRE - Monitor | Subscriptions";
  public description: string = "OpenAIRE - Monitor | Subscriptions";
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Subscriptions'}];
  public properties: EnvProperties = properties;
  subscriptions = [];
  
  constructor(
    protected _router: Router,
    protected _meta: Meta,
    protected _title: Title,
    protected seoService: SEOService,
    protected _piwikService: PiwikService,
    private helper: HelperService) {
    super();
  }

  ngOnInit() {
    this.setMetadata();
    this.getDivContents();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }

  private getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'monitor', this._router.url).subscribe(contents => {
      this.divContents = contents;
    }));
  }
}
