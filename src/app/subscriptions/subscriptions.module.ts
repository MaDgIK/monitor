import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {SubscriptionsComponent} from "./subscriptions.component";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {SliderTabsModule} from "../openaireLibrary/sharedComponents/tabs/slider-tabs.module";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";

@NgModule({
	declarations: [SubscriptionsComponent],
	imports: [CommonModule, RouterModule.forChild([
		{
			path: '',
			component: SubscriptionsComponent
		}
	]), BreadcrumbsModule, SliderTabsModule, HelperModule, IconsModule],
	exports: [SubscriptionsComponent],
	providers: [PreviousRouteRecorder, PiwikService]
})
export class SubscriptionsModule {}
