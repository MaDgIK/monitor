import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {ErrorMessagesModule} from '../openaireLibrary/utils/errorMessages.module';
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {OtherPortalsModule} from "../openaireLibrary/sharedComponents/other-portals/other-portals.module";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {HomeRoutingModule} from "./home-routing.module";
import {RefineFieldResultsServiceModule} from "../openaireLibrary/services/refineFieldResultsService.module";
import {SearchResearchResultsServiceModule} from "../openaireLibrary/services/searchResearchResultsService.module";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {UrlPrefixModule} from "../openaireLibrary/utils/pipes/url-prefix.module";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {SectionScrollModule} from '../openaireLibrary/utils/section-scroll/section-scroll.module';
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {BrowseStakeholderModule} from "../browse-stakeholder/browse-stakeholder.module";
import {SliderTabsModule} from '../openaireLibrary/sharedComponents/tabs/slider-tabs.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, ErrorMessagesModule, OtherPortalsModule,
    HelperModule, Schema2jsonldModule, SEOServiceModule, HomeRoutingModule, SearchResearchResultsServiceModule,
    RefineFieldResultsServiceModule, AlertModalModule, IconsModule, UrlPrefixModule, LogoUrlPipeModule,
		SectionScrollModule, LoadingModule, BrowseStakeholderModule, SliderTabsModule
  ],
  declarations: [
    HomeComponent
  ],
  providers: [
    PreviousRouteRecorder,
    PiwikService
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule {
  constructor() {}
}
