import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{HomeComponent} from './home.component';

import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {CanExitGuard} from "../openaireLibrary/utils/can-exit.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: HomeComponent, canDeactivate: [PreviousRouteRecorder, CanExitGuard] }

    ])
  ]
})
export class HomeRoutingModule { }
