import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {ManageModule} from "../openaireLibrary/utils/manage/manage.module";
import {LogoUrlPipeModule} from "../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {BrowseStakeholderComponent} from "./browse-stakeholder.component";
import {IconsService} from "../openaireLibrary/utils/icons/icons.service";
import {incognito, restricted} from "../openaireLibrary/utils/icons/icons";

@NgModule({
  imports: [
    CommonModule, RouterModule,
    ManageModule, AlertModalModule, LogoUrlPipeModule, IconsModule
  ],
  declarations: [
    BrowseStakeholderComponent
  ],
  providers:[
  ],
  exports: [
    BrowseStakeholderComponent
  ]
})
export class BrowseStakeholderModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([incognito, restricted]);
  }
}
