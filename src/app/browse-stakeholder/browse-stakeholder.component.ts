import {Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {StakeholderInfo, Visibility} from "../openaireLibrary/monitor/entities/stakeholder";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../environments/environment"
import {LocalStorageService} from "../openaireLibrary/services/localStorage.service";
import {Subscriber} from "rxjs";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";

@Component({
  selector: 'browse-stakeholder',
  templateUrl: 'browse-stakeholder.component.html',
  styleUrls: ['browse-stakeholder.component.less']
})
export class BrowseStakeholderComponent {
  @Input() public stakeholder: StakeholderInfo = null;
  @ViewChild('AlertModal', { static: true }) modal;
  public properties: EnvProperties = properties;
  public directLink: boolean = true;
  public visibilityIcon: Map<Visibility, string> = new Map<Visibility, string> ([
    ["PRIVATE", 'incognito'],
    ["RESTRICTED", 'restricted']
  ]);
  private subscriptions: any[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private localStorageService: LocalStorageService,
              private layoutService: LayoutService) {
  }


  public ngOnInit() {
    this.properties = properties;
    this.subscriptions.push(this.layoutService.isMobile.subscribe(value => {
      if(value) {
        this.directLink = true;
      } else {
        this.subscriptions.push(this.localStorageService.get().subscribe(value => {
          this.directLink = value;
        }));
      }
    }));
    

  }
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  mapType(type: string) {
		return StringUtils.getStakeholderType(type, false);
  }

  public confirmModalOpen() {
    this.modal.cancelButton = true;
    this.modal.okButton = true;
    this.modal.alertTitle = 'You are going to visit ' + this.stakeholder.name + ' Monitor Dashboard';
    this.modal.message = 'You will be navigated to a new tab. Are you sure that you want to proceed?';
    this.modal.okButtonLeft = false;
    this.modal.okButtonText = 'Yes';
    this.modal.cancelButtonText = 'No';
    this.modal.choice = true;
    this.modal.open();
  }
  
  public getStakeholderPageUrl() {
    return this.properties.domain + this.properties.baseLink + '/dashboard/' + this.stakeholder.alias;
  }
  
  hasPermission() {
    return this.stakeholder.visibility === "PUBLIC" ||
      (this.stakeholder.visibility === "RESTRICTED" && (this.stakeholder.isManager || this.stakeholder.isMember)) ||
      (this.stakeholder.visibility === "PRIVATE" && this.stakeholder.isManager);
  }
  
  public goToPage(data: any) {
    if (data.value == true) {
      let url = this.getStakeholderPageUrl();
      this.localStorageService.setCommunityDirectLink(data.choice);
      window.open(url, '_blank');
    }
  }
}
