import {Component} from '@angular/core';

@Component({
  selector: 'openaire-user',
  template: `<user></user>`
})

export class OpenaireUserComponent {

}

/*
import {Component, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {UserComponent} from '../openaireLibrary/login/user.component';
import {SubscribeService} from '../openaireLibrary/utils/subscribe/subscribe.service';
import {EmailService} from "../openaireLibrary/utils/email/email.service";

import {Session} from '../openaireLibrary/login/utils/helper.class';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {SubscribeComponent} from '../utils/subscribe/subscribe.component';
import {ConnectHelper} from '../openaireLibrary/connect/connectHelper';
import {properties} from "../../environments/environment";

@Component({
  selector: 'openaire-user',
  template: `
<div id="tm-main" class="uk-section  uk-margin-small-top tm-middle">

  <user [mainComponent]=false></user>
  <div *ngIf="!server" class=" uk-container uk-container-small uk-position-relative">

    <div *ngIf="usercomponent.errorCode == '6' && !isSubscribed" class="uk-alert uk-alert-warning">
      <span *ngIf="!isSubscribed ">For this action you have to <span *ngIf="!loggedIn">login and</span> subscribe to the research community.
        <span *ngIf="subscribe && !subscribeLoading "> <span *ngIf="!loggedIn">Login and </span>Subscribe <a [class]="'' + (subscribe.loading ? ' uk-disabled' : '')"  (click)="subscribeTo()"  > here</a>.</span>
      </span>
      <span *ngIf="subscribeLoading ">Subscribing to community....</span>
      <span *ngIf="subscribeError ">An error occured while trying to subscribe to community....</span>

    </div>
    <div *ngIf="usercomponent.errorCode == '7'" class="uk-alert uk-alert-warning">
      This action requires authentication.
      <span  *ngIf="!loggedIn">
        Please  <a  class=""  (click)="login()"  > sign in</a> to continue.
       </span>
    </div>


  </div>
</div>
    <subscribe  [communityId]="communityId" [showTemplate]= false class="" (subscribeEvent)="afterSubscibeEvent($event)"></subscribe>
    `
})

export class OpenaireUserComponent {
  @ViewChild(UserComponent) usercomponent:UserComponent;
  @ViewChild(SubscribeComponent) subscribe:SubscribeComponent;
  properties:EnvProperties;
  communityId = null;
  subscribeLoading:boolean = false;
  subscribeError:boolean = false;
  isSubscribed:boolean = false;
  public server: boolean = true;
  loggedIn:boolean = false;
  sub;
  constructor(private _subscribeService: SubscribeService,
              private _emailService: EmailService, private route: ActivatedRoute){}
  public ngOnInit() {
    if( typeof document !== 'undefined') {
      this.server = false;
      this.loggedIn = Session.isLoggedIn();
    }
    this.properties = properties;
    this.sub = this.route.queryParams.subscribe(
      communityId => {
        this.communityId  = ConnectHelper.getCommunityFromDomain(this.properties.domain);
        if(!this.communityId) {
          this.communityId = communityId['communityId'];
        }
        if(this.subscribe.subscribed){
          this.usercomponent.redirect();
        }
      });
  }
  login(){
    this.usercomponent.logIn();

  }
  subscribeTo(){
    if(this.subscribe && this.communityId){
      this.subscribeLoading = true;
      this.subscribe.subscribe();

    }

  }
  afterSubscibeEvent($event){
    var res = $event.value;
    this.subscribeLoading = false;
    this.isSubscribed = this.subscribe.subscribed;
    if(res == "ok"){
      this.isSubscribed = true;
      this.usercomponent.redirect();
    }else{
      this.subscribeError = true;
    }

  }

}
*/
