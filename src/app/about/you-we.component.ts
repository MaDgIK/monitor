import {
  AfterContentChecked, AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  ViewChild
} from "@angular/core";
import {StakeholderEntities} from "../openaireLibrary/monitor/entities/stakeholder";

declare var UIkit;

@Component({
  selector: 'you-we',
  template: `
    <div>
      <div class="uk-container">
        <h2 class="uk-text-center uk-margin-large-top uk-margin-large-bottom">Are you a <span
          [ngClass]="titleClass">{{type}}?</span></h2>
      </div>
      <div [id]="'sticky-' + id" #sticky class="uk-sticky uk-visible@l" [ngClass]="isSticky?backgroundClass:null"
           uk-sticky="animation: uk-animation-slide-bottom" [attr.end]="'#' + id"
           [attr.start]="'100vh -' + height + 'px'" [attr.offset]="offset">
        <div class="uk-container">
          <div class="uk-grid uk-grid-large uk-child-width-1-2@l" [class.uk-grid-divider]="!isSticky">
            <div class="uk-text-center uk-first-column">
              <h3 class="uk-margin-remove uk-margin-small-top uk-margin-small-bottom">You</h3>
            </div>
            <div class="uk-text-center">
              <h3 class="uk-margin-remove uk-margin-small-top uk-margin-small-bottom">We</h3>
            </div>
          </div>
        </div>
      </div>
      <div [id]="id" class="uk-container">
        <div class="uk-padding uk-padding-remove-vertical">
          <div class="uk-grid uk-grid-large uk-grid-divider uk-child-width-1-2@l" uk-grid
               uk-height-match="target: .uk-card">
            <div class="uk-flex uk-flex-column uk-flex-middle uk-child-width-1-1">
              <div class="uk-margin-top uk-margin-xlarge-bottom">
                <span class="uk-h3 uk-text-center uk-margin-bottom uk-hidden@l">You</span>
                <ng-content select="[you]"></ng-content>
              </div>
            </div>
            <div class="uk-flex uk-flex-column uk-flex-middle uk-child-width-1-1">
              <div class="uk-margin-top uk-margin-xlarge-bottom">
                <span class="uk-h3 uk-text-center uk-margin-bottom uk-hidden@l">We</span>
                <ng-content select="[we]"></ng-content>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class YouWeComponent implements AfterViewInit, AfterContentChecked, OnDestroy {
  @Input()
  public type: StakeholderEntities.FUNDER | StakeholderEntities.RI | StakeholderEntities.ORGANIZATION = StakeholderEntities.FUNDER;
  @Input()
  public id;
  @Input()
  titleClass: string = "uk-text-primary";
  @Input()
  backgroundClass: string = "uk-background-default uk-blur-background";
  @ViewChild('sticky') sticky: ElementRef;
  public isSticky: boolean = false;
  public offset: number
  public height: number;
  private subscriptions: any[] = [];
  
  constructor(private cdr: ChangeDetectorRef) {
  }
  
  ngAfterViewInit() {
    if (this.sticky) {
      this.observeSticky();
    }
  }
  
  ngAfterContentChecked() {
    if (this.sticky && typeof document !== 'undefined') {
      this.offset = this.calcOffset(this.sticky.nativeElement);
    }
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof (ResizeObserver || IntersectionObserver)) {
        subscription.disconnect();
      }
    });
  }
  
  public observeSticky() {
    let resizeObs = new ResizeObserver(entries => {
      entries.forEach(entry => {
        setTimeout(() => {
          this.offset = this.calcOffset(entry.target);
          this.cdr.detectChanges();
        });
      })
    });
    this.subscriptions.push(resizeObs);
    resizeObs.observe(this.sticky.nativeElement);
    this.subscriptions.push(UIkit.util.on('#sticky-' + this.id, 'active', (): void => {
      this.isSticky = true;
    }));
    this.subscriptions.push(UIkit.util.on('#sticky-' + this.id, 'inactive', () => {
      this.isSticky = false;
    }));
  }
  
  calcOffset(element) {
    this.height = element.offsetHeight;
    return window.innerHeight - this.height;
  }
}
