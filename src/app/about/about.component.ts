import {ChangeDetectorRef, Component} from "@angular/core";
import {Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {Subscriber} from "rxjs";
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../environments/environment";
import {StakeholderEntities} from "../openaireLibrary/monitor/entities/stakeholder";
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";

@Component({
  selector: 'about',
  template: `
		<div class="uk-visible@m">
			<div class="uk-position-relative">
				<div class="uk-background-muted">
					<div class="uk-container uk-container-large uk-section uk-section-small">
						<div class="uk-padding-small uk-padding-remove-horizontal">
							<breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
						</div>
						<div class="uk-flex uk-flex-column uk-flex-center uk-margin-bottom" uk-scrollspy-class>
							<h1 class="uk-margin-medium-top" uk-scrollspy-class>How it works<span class="uk-text-primary">.</span></h1>
							<div class="uk-text-large uk-width-1-2@l uk-width-2-3" uk-scrollspy-class>
								Join the OpenAIRE Monitor service and we will create for you a dashboard to track, understand and
								position your organization's research activities and their impact, discover and evaluate Open Science
								trends for your organization and make data-driven decisions. Here's how it works.
							</div>
							<div class="uk-margin-medium-top uk-margin-small-bottom" uk-scrollspy-class>
								<a class="uk-button uk-button-primary uk-text-uppercase" routerLink="/get-started">Get Started</a>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-padding"></div>
				<img class="uk-position-bottom-right uk-width-1-3@xl uk-width-1-2@l"
							src="assets/monitor-assets/about/hero.svg" loading="lazy">
			</div>
			<div class="uk-section uk-section-small">
				<div class="uk-sticky uk-blur-background uk-visible@l" [attr.uk-sticky]="shouldSticky?'':null" [attr.offset]="offset">
					<div class="uk-container">
						<div class="uk-padding uk-padding-remove-right uk-padding-remove-bottom">
							<slider-tabs flexPosition="center" customClass="uk-text-large" connect="#tabs-content">
								<slider-tab *ngFor="let section of sections; let i = index" [tabTitle]="section"
														[tabId]="section"></slider-tab>
							</slider-tabs>
						</div>
					</div>
				</div>
				<ul id="tabs-content" class="uk-switcher uk-margin-large-bottom">
					<li>
						<you-we [type]="stakeholderEntities.FUNDER" id="funder">
							<ng-container you>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-large-bottom">
										<span class="uk-text-primary">Step 1a</span>
										<p class="uk-text-large uk-margin-xsmall-top">You provide Information about your funded projects and
											<a href="https://www.openaire.eu/funders-how-to-join-guide" target="_blank">join OpenAIRE</a>.</p>
										<p>All information is exchanged under confidential agreements and we only make public what you agree
											on.</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-left custom-translate-bottom-left"
												src="assets/monitor-assets/about/icon1.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 2a</span>
									<p class="uk-text-large uk-margin-xsmall-top">You validate the results and approve for them to be shown
										on the <a href="https://explore.openaire.eu" target="_blank">Explore</a> portal.</p>
									<p>Examine your dashboard and the showcased information to ensure you are satisfied with what you
										see.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 3a</span>
									<p class="uk-text-large uk-margin-xsmall-top">Through 1-on-1 expert consultations you interact with
										us</p>
									<ul class="uk-list uk-list-disc">
										<li>to understand any gaps and deviations within the dashboard</li>
										<li>to verify the data integrity</li>
										<li>to discuss the addition of any new metrics and indicators that might interest you</li>
									</ul>
								</div>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-medium-bottom">
										<span class="uk-text-primary">Step 4a</span>
										<p class="uk-text-large uk-margin-xsmall-top">Customise your dashboard and make your own profile.</p>
										<p>Select the <a href="https://monitor.openaire.eu/indicators/funder" target="_blank">metrics</a> of
											interest to you and the ones you want to publish, to keep private, or to share just with colleagues.
										</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-left" src="assets/monitor-assets/about/icon3.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative">
									<span class="uk-text-primary">Step 5a</span>
									<p class="uk-text-large uk-margin-xsmall-top">Select your team</p>
									<p>And make them collaborators of the service.</p>
								</div>
							</ng-container>
							<ng-container we>
								<div class="uk-card uk-card-default uk-overflow-hidden uk-position-relative uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-large-bottom">
										<span class="uk-text-primary">Step 1b</span>
										<p class="uk-text-large uk-margin-xsmall-top">We ingest your project metadata and mine the <a
												href="https://graph.openaire.eu" target="_blank" class="text-graph">OpenAIRE Graph</a>.
										</p>
										<p>We infer links to research results: publications, datasets, software, etc.</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-right" src="assets/monitor-assets/about/icon2.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 2b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We present the results to you on our Beta portal.</p>
									<p>We refine until you are happy with the results. We allocate the hardware resources needed to create
										your dashboard.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 3b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We produce well-documented visualisations of simple & more
										advanced composite indicators.</p>
									<p>Aligned with the Open Science principles and following an evidence-based approach, and based on the
										funded research activities.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 4b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We refine until we are happy with the results.</p>
									<p>We present your dashboard to you.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative">
									<span class="uk-text-primary">Step 5b</span>
									<p class="uk-text-large uk-margin-xsmall-top">Notify you about new metrics and indicators</p>
									<p>as we keep enriching the Graph, which you may find useful and add to your profile</p>
								</div>
							</ng-container>
						</you-we>
					</li>
					<li>
						<you-we [type]="stakeholderEntities.RI" id="ri">
							<ng-container you>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-large-bottom">
										<span class="uk-text-primary">Step 1a</span>
										<p class="uk-text-large uk-margin-xsmall-top">You state your interest in having a research initiative
											dashboard.</p>
										<p>All information is exchanged under confidential agreements and we only make public what you agree
											on.</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-left custom-translate-bottom-left"
												src="assets/monitor-assets/about/icon1.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 2a</span>
									<p class="uk-text-large uk-margin-xsmall-top">You configure the criteria to identify the research
										products that you want to be accounted for.</p>
									<p>You use the administration dashboard we created for you. If this is not enough, you give us input for
										the implementation of a customised full-text mining algorithm.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 3a</span>
									<p class="uk-text-large uk-margin-xsmall-top">Through 1-on-1 expert consultations you interact with
										us</p>
									<ul class="uk-list uk-list-disc">
										<li>to understand any gaps and deviations within the dashboard</li>
										<li>to verify the data integrity</li>
										<li>to discuss the addition of any new metrics and indicators that might interest you</li>
									</ul>
								</div>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-medium-bottom">
										<span class="uk-text-primary">Step 4a</span>
										<p class="uk-text-large uk-margin-xsmall-top">Customise your dashboard and make your own profile.</p>
										<p>Select the <a href="https://monitor.openaire.eu/indicators/ri" target="_blank">metrics</a> of
											interest to you and the ones you want to publish, to keep private, or to share just with colleagues.
										</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-left" src="assets/monitor-assets/about/icon3.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative">
									<span class="uk-text-primary">Step 5a</span>
									<p class="uk-text-large uk-margin-xsmall-top">Select your team</p>
									<p>And make them collaborators of the service.</p>
								</div>
							</ng-container>
							<ng-container we>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-large-bottom">
										<span class="uk-text-primary">Step 1b</span>
										<p class="uk-text-large uk-margin-xsmall-top">We set up an administration dashboard that you can use
											to specify which research products of the <a href="https://graph.openaire.eu" target="_blank"
																																		class="text-graph">OpenAIRE Graph</a> are
											relevant for you.</p>
										<p>If needed, we develop a customized full-text mining algorithm to identify your publications.</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-right" src="assets/monitor-assets/about/icon2.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 2b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We present the results to you on our Beta portal.</p>
									<p>We refine until you are happy with the results. We allocate the hardware resources needed to create
										your dashboard.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 3b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We produce well-documented visualisations of simple & more
										advanced composite indicators.</p>
									<p>Aligned with the Open Science principles and following an evidence-based approach.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 4b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We refine until we are happy with the results.</p>
									<p>We present your dashboard to you.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative">
									<span class="uk-text-primary">Step 5b</span>
									<p class="uk-text-large uk-margin-xsmall-top">Notify you about new metrics and indicators</p>
									<p>as we keep enriching the Graph, which you may find useful and add to your profile</p>
								</div>
							</ng-container>
						</you-we>
					</li>
					<li>
						<!-- RESEARCH INSTITUTION -->
						<you-we [type]="stakeholderEntities.ORGANIZATION" id="organizations">
							<ng-container you>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-large-bottom">
										<span class="uk-text-primary">Step 1a</span>
										<p class="uk-text-large uk-margin-xsmall-top">You state your interest in having an institutional
											dashboard for your institution.</p>
										<p>All information is exchanged under confidential agreements and we only make public what you agree
											on.</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-left custom-translate-bottom-left"
												src="assets/monitor-assets/about/icon1.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 2a</span>
									<p class="uk-text-large uk-margin-xsmall-top">You validate and approve the results.</p>
									<p>You examine your dashboard and the showcased information to ensure you are satisfied with what you
										see.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 3a</span>
									<p class="uk-text-large uk-margin-xsmall-top">Through 1-on-1 expert consultations you interact with
										us</p>
									<ul class="uk-list uk-list-disc">
										<li>to understand any gaps and deviations within the dashboard</li>
										<li>to verify the data integrity</li>
										<li>to discuss the addition of any new metrics and indicators that might interest you</li>
									</ul>
								</div>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-medium-bottom">
										<span class="uk-text-primary">Step 4a</span>
										<p class="uk-text-large uk-margin-xsmall-top">Customise your dashboard and make your own profile.</p>
										<p>Select the <a href="https://monitor.openaire.eu/indicators/organization" target="_blank">metrics</a> of
											interest to you and the ones you want to publish, to keep private, or to share just with colleagues.
										</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-left" src="assets/monitor-assets/about/icon3.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative">
									<span class="uk-text-primary">Step 5a</span>
									<p class="uk-text-large uk-margin-xsmall-top">Select your team</p>
									<p>And make them collaborators of the service.</p>
								</div>
							</ng-container>
							<ng-container we>
								<div class="uk-card uk-card-default uk-position-relative uk-overflow-hidden uk-margin-large-bottom">
									<div class="uk-card-body uk-margin-large-bottom">
										<span class="uk-text-primary">Step 1b</span>
										<p class="uk-text-large uk-margin-xsmall-top">Starting from the
                      <a href="https://graph.openaire.eu" target="_blank" class="text-graph">OpenAIRE Graph</a> we check for existing data for your institution.</p>
										<p>We infer links to research results: publications, datasets, software, etc.</p>
									</div>
									<img class="uk-width-xsmall uk-position-bottom-right" src="assets/monitor-assets/about/icon2.svg">
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 2b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We present the results to you on our Beta portal.</p>
									<p>We refine until you are happy with the results. We allocate the hardware resources needed to create
										your dashboard.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 3b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We produce well-documented visualisations of simple & more
										advanced composite indicators.</p>
									<p>Aligned with the Open Science principles and following an evidence-based approach, and based on the
										institution’s research activities.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
									<span class="uk-text-primary">Step 4b</span>
									<p class="uk-text-large uk-margin-xsmall-top">We refine until we are happy with the results.</p>
									<p>We present your dashboard to you.</p>
								</div>
								<div class="uk-card uk-card-default uk-card-body uk-position-relative">
									<span class="uk-text-primary">Step 5b</span>
									<p class="uk-text-large uk-margin-xsmall-top">Notify you about new metrics and indicators</p>
									<p>as we keep enriching the Graph, which you may find useful and add to your profile</p>
								</div>
							</ng-container>
						</you-we>
					</li>
				</ul>
			</div>
		</div>
		<div class="uk-hidden@m">
			<div class="uk-background-muted">
				<div class="uk-container uk-container-large">
					<div class="uk-text-center" uk-scrollspy-class>
						<h1 class="uk-margin-medium-top uk-margin-medium-bottom" uk-scrollspy-class>How it works<span class="uk-text-primary">.</span></h1>
						<div class="uk-text-large" uk-scrollspy-class>
							Join the OpenAIRE Monitor service and we will create for you a dashboard to track, understand and
							position your organization's research activities and their impact, discover and evaluate Open Science
							trends for your organization and make data-driven decisions. Here's how it works.
						</div>
						<div class="uk-margin-medium-top" uk-scrollspy-class>
							<a class="uk-button uk-button-primary uk-text-uppercase" routerLink="/get-started">Get Started</a>
						</div>
					</div>
				</div>
				<div class="uk-position-relative uk-height-medium" style="overflow-x: hidden; overflow-x: clip;">
					<img class="uk-width-large uk-position-absolute" src="assets/monitor-assets/about/hero.svg" loading="lazy"
						style="bottom: -70px; right: -80px;">
				</div>
					<!-- style="transform: translate(60px, 40px);" -->
			</div>
			<div class="uk-section uk-section-large uk-margin-medium-top">
				<div class="uk-sticky uk-blur-background" [attr.uk-sticky]="shouldSticky?'':null">
					<div class="uk-padding-small uk-padding-remove-right uk-padding-remove-bottom">
						<slider-tabs flexPosition="center" customClass="uk-text-large" connect="#tabs-content-mobile">
							<slider-tab *ngFor="let section of sections; let i = index" [tabTitle]="section"
													[tabId]="section"></slider-tab>
						</slider-tabs>
					</div>
				</div>
				<div class="uk-container uk-container-large">
					<ul id="tabs-content-mobile" class="uk-switcher uk-margin-large-bottom">
						<li id="funder">
							<div class="uk-h1 uk-text-center uk-margin-xlarge-top uk-margin-xlarge-bottom">
								Are you a <span class="uk-text-primary">Funder?</span>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 1a</span>
									<h5 class="uk-margin-xsmall-top">You provide Information about your funded projects and
										<a href="https://www.openaire.eu/funders-how-to-join-guide" target="_blank">join OpenAIRE</a>.</h5>
									<p>All information is exchanged under confidential agreements and we only make public what you agree
										on.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 1b</span>
									<h5 class="uk-margin-xsmall-top">We ingest your project metadata and mine the <a
											href="https://graph.openaire.eu" target="_blank" class="text-graph">OpenAIRE Graph</a>.
									</h5>
									<p>We infer links to research results: publications, datasets, software, etc.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 2a</span>
									<h5 class="uk-margin-xsmall-top">You validate the results and approve for them to be shown
										on the <a href="https://explore.openaire.eu" target="_blank">Explore</a> portal.</h5>
									<p>Examine your dashboard and the showcased information to ensure you are satisfied with what you
										see.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 2b</span>
									<h5 class="uk-margin-xsmall-top">We present the results to you on our Beta portal.</h5>
									<p>We refine until you are happy with the results. We allocate the hardware resources needed to create
										your dashboard.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 3a</span>
									<h5 class="uk-margin-xsmall-top">Through 1-on-1 expert consultations you interact with
										us</h5>
									<ul class="uk-list uk-list-disc">
										<li>to understand any gaps and deviations within the dashboard</li>
										<li>to verify the data integrity</li>
										<li>to discuss the addition of any new metrics and indicators that might interest you</li>
									</ul>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 3b</span>
									<h5 class="uk-margin-xsmall-top">We produce well-documented visualisations of simple & more
										advanced composite indicators.</h5>
									<p>Aligned with the Open Science principles and following an evidence-based approach, and based on the
										funded research activities.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 4a</span>
									<h5 class="uk-margin-xsmall-top">Customise your dashboard and make your own profile.</h5>
									<p>Select the <a href="https://monitor.openaire.eu/indicators/funder" target="_blank">metrics</a> of
										interest to you and the ones you want to publish, to keep private, or to share just with colleagues.
									</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 4b</span>
									<h5 class="uk-margin-xsmall-top">We refine until we are happy with the results.</h5>
									<p>We present your dashboard to you.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 5a</span>
									<h5 class="uk-margin-xsmall-top">Select your team</h5>
									<p>And make them collaborators of the service.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 5b</span>
									<h5 class="uk-margin-xsmall-top">Notify you about new metrics and indicators</h5>
									<p>as we keep enriching the Graph, which you may find useful and add to your profile</p>
								</div>
							</div>
						</li>
						<li id="ri">
							<div class="uk-h1 uk-text-center uk-margin-xlarge-top uk-margin-xlarge-bottom">
								Are you a <span class="uk-text-primary">Research Initiative?</span>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 1a</span>
									<h5 class="uk-margin-xsmall-top">You state your interest in having a research initiative
										dashboard.</h5>
									<p>All information is exchanged under confidential agreements and we only make public what you agree
										on.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 1b</span>
									<h5 class="uk-margin-xsmall-top">We set up an administration dashboard that you can use
										to specify which research products of the <a href="https://graph.openaire.eu" target="_blank"
																																	class="text-graph">OpenAIRE Graph</a> are
										relevant for you.</h5>
									<p>If needed, we develop a customized full-text mining algorithm to identify your publications.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 2a</span>
									<h5 class="uk-margin-xsmall-top">You configure the criteria to identify the research
										products that you want to be accounted for.</h5>
									<p>You use the administration dashboard we created for you. If this is not enough, you give us input for
										the implementation of a customised full-text mining algorithm.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 2b</span>
									<h5 class="uk-margin-xsmall-top">We present the results to you on our Beta portal.</h5>
									<p>We refine until you are happy with the results. We allocate the hardware resources needed to create
										your dashboard.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 3a</span>
									<h5 class="uk-margin-xsmall-top">Through 1-on-1 expert consultations you interact with
										us</h5>
									<ul class="uk-list uk-list-disc">
										<li>to understand any gaps and deviations within the dashboard</li>
										<li>to verify the data integrity</li>
										<li>to discuss the addition of any new metrics and indicators that might interest you</li>
									</ul>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 3b</span>
									<h5 class="uk-margin-xsmall-top">We produce well-documented visualisations of simple & more
										advanced composite indicators.</h5>
									<p>Aligned with the Open Science principles and following an evidence-based approach, and based on the
										institution’s research activities.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 4a</span>
									<h5 class="uk-margin-xsmall-top">Customise your dashboard and make your own profile.</h5>
									<p>Select the <a href="https://monitor.openaire.eu/indicators/ri" target="_blank">metrics</a> of
										interest to you and the ones you want to publish, to keep private, or to share just with colleagues.
									</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 4b</span>
									<h5 class="uk-margin-xsmall-top">We refine until we are happy with the results.</h5>
									<p>We present your dashboard to you.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 5a</span>
									<h5 class="uk-margin-xsmall-top">Select your team</h5>
									<p>And make them collaborators of the service.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 5b</span>
									<h5 class="uk-margin-xsmall-top">Notify you about new metrics and indicators</h5>
									<p>as we keep enriching the Graph, which you may find useful and add to your profile</p>
								</div>
							</div>
						</li>
						<li id="organizations">
							<div class="uk-h1 uk-text-center uk-margin-xlarge-top uk-margin-xlarge-bottom">
								Are you a <span class="uk-text-primary">Research Institution?</span>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 1a</span>
									<h5 class="uk-margin-xsmall-top">You state your interest in having an institutional
										dashboard for your institution.</h5>
									<p>All information is exchanged under confidential agreements and we only make public what you agree
										on.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 1b</span>
									<h5 class="uk-margin-xsmall-top">Starting from the <a href="https://graph.openaire.eu"
																																											target="_blank" class="text-graph">OpenAIRE
										Research Graph</a> we check for existing data for your institution.</h5>
									<p>We infer links to research results: publications, datasets, software, etc.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 2a</span>
									<h5 class="uk-margin-xsmall-top">You validate and approve the results.</h5>
									<p>You examine your dashboard and the showcased information to ensure you are satisfied with what you
										see.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 2b</span>
									<h5 class="uk-margin-xsmall-top">We present the results to you on our Beta portal.</h5>
									<p>We refine until you are happy with the results. We allocate the hardware resources needed to create
										your dashboard.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 3a</span>
									<h5 class="uk-margin-xsmall-top">Through 1-on-1 expert consultations you interact with
										us</h5>
									<ul class="uk-list uk-list-disc">
										<li>to understand any gaps and deviations within the dashboard</li>
										<li>to verify the data integrity</li>
										<li>to discuss the addition of any new metrics and indicators that might interest you</li>
									</ul>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 3b</span>
									<h5 class="uk-margin-xsmall-top">We produce well-documented visualisations of simple & more
										advanced composite indicators.</h5>
									<p>Aligned with the Open Science principles and following an evidence-based approach, and based on the
										institution’s research activities.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 4a</span>
									<h5 class="uk-margin-xsmall-top">Customise your dashboard and make your own profile.</h5>
									<p>Select the <a href="https://monitor.openaire.eu/indicators/organization" target="_blank">metrics</a> of
										interest to you and the ones you want to publish, to keep private, or to share just with colleagues.
									</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 4b</span>
									<h5 class="uk-margin-xsmall-top">We refine until we are happy with the results.</h5>
									<p>We present your dashboard to you.</p>
								</div>
							</div>
							<div class="uk-card uk-card-default uk-card-body uk-position-relative uk-margin-large-bottom">
								<div>
									<span class="uk-text-primary">Step 5a</span>
									<h5 class="uk-margin-xsmall-top">Select your team</h5>
									<p>And make them collaborators of the service.</p>
								</div>
								<div class="uk-margin-large-top">
									<span class="uk-text-primary">Step 5b</span>
									<h5 class="uk-margin-xsmall-top">Notify you about new metrics and indicators</h5>
									<p>as we keep enriching the Graph, which you may find useful and add to your profile</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
  `,
  styleUrls: ['about.component.less']
})
export class AboutComponent {
  public url: string = null;
  public pageTitle: string = "OpenAIRE - Monitor | About";
  public description: string = "OpenAIRE - Monitor | About - How it works";
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'About'}];
  public properties: EnvProperties = properties;
  public sections: string[] = [StakeholderEntities.FUNDERS, StakeholderEntities.RIS, StakeholderEntities.ORGANIZATIONS];
  public offset: number;
  public stakeholderEntities = StakeholderEntities;
  public shouldSticky: boolean = true;
  subscriptions = [];
  
  constructor(
    private _router: Router,
    private _meta: Meta,
    private _title: Title,
    private seoService: SEOService,
    private _piwikService: PiwikService,
    private layoutService: LayoutService,
    private helper: HelperService,
    private cdr: ChangeDetectorRef) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this._piwikService.trackView(this.properties, this.pageTitle).subscribe());
    this.url = this.properties.domain + this.properties.baseLink + this._router.url;
    this.seoService.createLinkForCanonicalURL(this.url);
    this.updateUrl(this.url);
    this.updateTitle(this.pageTitle);
    this.updateDescription(this.description);
  }
  
  ngAfterViewInit() {
    this.subscriptions.push(this.layoutService.isBottomIntersecting.subscribe(isBottomIntersecting => {
      this.shouldSticky = !isBottomIntersecting;
      this.cdr.detectChanges();
    }));
    if (typeof document !== 'undefined') {
      this.offset = Number.parseInt(getComputedStyle(document.documentElement).getPropertyValue('--header-height'));
      this.cdr.detectChanges();

    }
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }
  
  private updateTitle(title: string) {
    var _title = ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(_title);
    this._meta.updateTag({content: _title}, "property='og:title'");
  }
  
  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }
}
