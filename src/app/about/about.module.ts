import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {AboutComponent} from "./about.component";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {SliderTabsModule} from "../openaireLibrary/sharedComponents/tabs/slider-tabs.module";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {YouWeComponent} from "./you-we.component";

@NgModule({
	declarations: [AboutComponent, YouWeComponent],
	imports: [CommonModule, RouterModule.forChild([
		{
			path: '',
			component: AboutComponent,
			data: {extraOffset: 50}
		},
		{
			path: 'how-it-works',
			redirectTo: '/about'
		},
		{
			path: 'faqs',
			redirectTo: '/support'
		}
	]), BreadcrumbsModule, SliderTabsModule, HelperModule],
	exports: [AboutComponent],
	providers: [PreviousRouteRecorder, PiwikService]
})
export class AboutModule {}
