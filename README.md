# Monitor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.10 and has been updated to 16.2.4.

## Install packages

Run npm install (maybe needs sudo), a script that will delete unused files from library will be run.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4500/`. The app will automatically reload if you change any of the source files.

## Build - CSR

Use the `npm run build-dev` for a development build.<br>
Use the `npm run build-beta` for a beta build.<br>
Use the `npm run build-prod` for a production build.

## Build - SSR

Use the `npm run build:ssr-dev` for a development build.<br>
Use the `npm run build:ssr-beta` for a beta build.<br>
Use the `npm run build:ssr-prod` for a production build.

## Run SSR

`npm run serve:ssr` will run the last server build.

## Webpack Analyzer

In order to analyze bundle size you can run `npm run webpack-bundle-analyzer`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
